FROM debian:bullseye-slim

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

RUN apt-get update \
	&& apt-get install -y bup mariadb-client \
	&& rm -rf /var/lib/apt/lists/*

COPY docker-entry.sh /usr/bin

CMD ["/usr/bin/docker-entry.sh"]
