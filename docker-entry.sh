#!/bin/bash

# Initialise bup repository when run for the first time
[ -f /root/.bup/config ] || bup init

# Dump database
if test -n "${MYSQL_DATABASE}" -a -n "${MYSQL_USER}" -a -n "${MYSQL_PASSWORD}" -a -n "${MYSQL_HOST}"; then
	echo Dumping database $MYSQL_DATABASE
	mysqldump --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} --single-transaction ${MYSQLDUMP_PARAMETERS} ${MYSQL_DATABASE} | sed -e 's/),(/),\n(/g' > /backup/${MYSQL_DATABASE}.sql

	# Try again in one minute if the database server is not yet fully started
	if test $? > 0; then
		echo Database server is down, restarting in 1 minute
		sleep 1m;
		echo Dumping database $MYSQL_DATABASE
		mysqldump --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} --single-transaction ${MYSQLDUMP_PARAMETERS} ${MYSQL_DATABASE} | sed -e 's/),(/),\n(/g' > /backup/${MYSQL_DATABASE}.sql
	fi
fi

bup index --indexfile /root/.bup/${SAC}index /backup
bup save --name ${SAC} --strip --indexfile /root/.bup/${SAC}index --verbose --verbose /backup
